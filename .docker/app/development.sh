#!/bin/bash

# rm server.pid
rm -f /app/tmp/pids/server.pid

# run server
bundle exec rails server -b 0.0.0.0
