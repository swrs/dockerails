ARG RUBY_VERSION
FROM ruby:$RUBY_VERSION-slim-buster

ARG PG_MAJOR
ARG NODE_MAJOR
ARG BUNDLER_VERSION
ARG RAILS_VERSION
ARG YARN_VERSION

# container user with user id (USER_ID) and group id (GROUP_ID)
ARG USER_NAME=container
ARG USER_ID
ARG GROUP_ID

# RAILS_ENV is used to install gems depending on the
# desired rails environment
ARG RAILS_ENV=development

# common dependencies
RUN apt-get update -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      build-essential \
      gnupg2 \
      curl \
      less \
      git \
    && apt-get clean \
    && rm -rf /var/cache/apt/archives/* \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && truncate -s 0 /var/log/*log

# get postgresql version for apt-get install
RUN curl -sSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && echo 'deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main' $PG_MAJOR > /etc/apt/sources.list.d/pgdg.list

# get nodejs version for apt-get install
RUN curl -sL https://deb.nodesource.com/setup_$NODE_MAJOR.x | bash -

# get yarn version for apt-get install
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo 'deb http://dl.yarnpkg.com/debian/ stable main' > /etc/apt/sources.list.d/yarn.list

# install further dependencies
COPY .docker/app/aptfile /tmp/aptfile
RUN apt-get update -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get -yq dist-upgrade \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      libpq-dev \
      postgresql-client-$PG_MAJOR \
      nodejs \
      yarn=$YARN_VERSION-1 \
      $(cat /tmp/aptfile | xargs) \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && truncate -s 0 /var/log/*log

# install libvips
RUN curl -L -O https://github.com/libvips/libvips/releases/download/v8.9.2/vips-8.9.2.tar.gz
RUN tar xf vips-8.9.2.tar.gz \
    && cd vips-8.9.2 \
    && ./configure \
    && make && make install && ldconfig \
    && cd ../ \
    && rm -rf vips-8.9.2 vips-8.9.2.tar.gz

# bundler config
ENV LANG=C.UTF-8 \
  BUNDLE_JOBS=4 \
  BUNDLE_RETRY=3

# store bundler config in project root
ENV BUNDLE_APP_CONFIG=.bundle

# Uncomment this line if you want to run binstubs without prefixing with `bin/` or `bundle exec`
# ENV PATH /app/bin:$PATH

# install bundler
RUN gem update --system \
    && gem install bundler -v $BUNDLER_VERSION \
    && gem install rails -v $RAILS_VERSION

# add non-root group and user, create workdir and dirs 
# create workdirs as root user
RUN mkdir /app

# add container user and change ownership of working dirs
RUN groupadd -f -g ${GROUP_ID} ${USER_NAME} \
    && useradd -m -u ${USER_ID} -g ${GROUP_ID} -s /bin/bash ${USER_NAME} \
    && chown ${USER_ID}:${GROUP_ID} /app \
    && chown ${USER_ID}:${GROUP_ID} /usr/local/bundle \
    && chown -R ${USER_ID}:${GROUP_ID} /usr/local/bundle

# change user to container user
USER ${USER_NAME}

# set working directory
WORKDIR /app

CMD ["/bin/bash"]
