#!/bin/bash

# bundle install and yarn install 
bundle install
yarn install

# create db and migrate
rails db:create db:migrate

# precompile assets
bundle exec rails assets:precompile

# rm server.pid
rm -f /app/tmp/pids/server.pid

# run server
bundle exec rails server -b 0.0.0.0
