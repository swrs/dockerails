# Dockerails

*Dockerails* is a [docker](https://www.docker.com/)ized [Ruby on Rails](https://rubyonrails.org/) setup, let's say eco-system, featuring a containerized development workflow, redis server, webpacker dev server, sidekiq background jobs, libvips image processing, and a nginx reverse proxy setup with caching. It is heavily inspired by [Vladimir Dementyev's setup](https://evilmartians.com/chronicles/ruby-on-whales-docker-for-ruby-rails-development).

## Requirements

- [Docker Engine](https://www.docker.com/) >= 19.03.0
- [Docker Compose](https://docs.docker.com/compose/) >= 3.8

## Setup

### Installation and initialization

Install the repo using `git clone`. If you want to change the setup (most likely), remove `.git`, etc. and put your setup under version control.

```bash
$ git clone https://gitlab.com/swrs/dockerails myapp
$ cd myapp

# optional
rm -r .git .gitignore
git init
```

To build the docker containers, you will need a couple of build arguments to define versions of installed packages and user permissions. Those arguments are either set in `docker-compose.yml` or via environment variables. 

The necessary environment variables are `USER_ID` and `GROUP_ID` which will set the user and group ids within the docker containers. This will avoid using users with root privileges, a convenient method is to use your host's user. Finally build…

```bash
$ export USER_ID=$(id -u)
$ export GROUP_ID=$(id -g)
$ docker-compose build

# or

$ USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose build
```

All mounted volumes will have to be owned by the "container" user which *might* have to be fixed due to the everlasting confusion around ownership of mounted volumes.

```bash
docker-compose run -u root runner chown -R $USER_ID:$GROUP_ID /app/public/uploads /app/public/packs /app/tmp /app/node_modules
```

### Build arguments and environment variables

The `app` and `postgres` images need environment variables defined in `.docker/app/.env` and `.docker/postgres/.env` files. You can use the provided `.env.sample` files by renaming them to `.env`. 

The environment variables are mostly used to set the *Node* and *Rails* environments (*development*, *production*, etc.) and set a password for the postgres database. 

### Application initialization

Initialize your rails application with the `rails new .` command from the `runner` service using `--database-postgresql`, `--webpacker`, `--skip-sprockets` (using webpacker for all assets), and `--skip-git`. In order to avoid permission problems with mounted docker volumes you should create directories which will be mounted with your system user before running `rails new …`. 

```bash
mkdir -p public/{packs,uploads} node_modules tmp
docker-compose run --rm runner rails new . --webpacker --skip-sprockets --all-those-skipping-options
```

As you skipped git during `rails new`, you can put the whole dockerized application under version control.

```bash
git init
```

### Application configuration (database, etc.)

In order to work with the dockerizd environment, the application will have to know where to find the database, redis, the webpack-dev-server, …, thus some additional configuration is necessary. Basically all necessary services are available from within the docker network – yes, this can be confusing. Normally a service is exposed on a host name equal to the service name, thus the `webpacker` service is exposed at `http://webpacker:3035` (again, from WITHIN the docker network).

Configure the database in `config/database.yml` to set the postgres host, username and password.

```yml
default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: <%= ENV.fetch("POSTGRES_HOST") %>
  username: <%= ENV.fetch("POSTGRES_USER") %>
  password: <%= ENV.fetch("POSTGRES_PASSWORD") %>
```

Use the redis service wherever it is necessary, such as in `config/cable.yml`. The redis server is exposed at `redis://redis:6379` and the `/0` path refers to the database `0`.

```yml
production:
  adapter: redis
  url: "redis://redis:6379/0"
  channel_prefix: sgp_production
```

In development you might want to run the `webpacker` service which runs `webpack-dev-server` and *Webpacker* will have to know where the dev server is accessible. In `config/webpacker.yml` config the dev server.

```yml

development:
  # …
  dev_server:
    # … 
    host: webpacker
    port: 3035
    public: 0.0.0.0:3035
```

## Usage

Run Rails server and all services it depends on:

```bash
docker-compose up rails
```

If you want to make use of debugging using the *byebug* gem, you'll have to run the rails container in detached mode and attach to it "manually":

```bash
docker-compose up -d rails && docker attach <rails container_name>
```

Use the runner to execute command within the rails environment:

```bash
$ docker-compose run --rm runner rails db:create db:migrate
$ docker-compose run --rm runner rails test
$ docker-compose run --rm runner rails mytask
# etc. etc. etc.
```

Run Rails and Nginx servers in order to test a reverse proxy setup. This could come in handy to simulate content caching via Nginx, etc.:

```bash
docker-compose up rails nginx
```

To use different setups for `development`, `production`, … environments, you can make use of *Docker Compose*'s function to override compose file setups. With the following command, settings defined in `docker-compose.production.yml` will take precedence over settings defined in `docker-compose.yml`.

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.production.yml up rails
```

## Workflow

A common development workflow could be to use several shells for background services, rails server, and the runner.

```bash
# shell 1
docker-compose up postgres webpacker redis sidekiq
# shell 2
docker-compose up -d rails nginx && docker attach <rails container name>
# shell #
docker-compose run --rm runner
```

The last `runner` service can be used to execute `bundle install`s, `rails db:migrate`s, or the recurring `rails test`.

## Structure and configuration

### Services

- `docker-compose run --rm runner`: Application environment to run individual commands, such as `bundle install`, or `rails db:migrate`.
- `docker-compose up rails`: Rails server on port 3000.
- `docker-compose up webpacker`: Webpacker dev server on port 3035.
- `docker-compose up sidekiq`: Sidekiq service (no port exposed).
- `docker-compose up redis`: Redis server (no port exposed).
- `docker-compose up nginx`: Nginx proxy reverse server for Rails server on port 8081.

### Directory and volume setup

The whole application codebase lives in  the `app/` directory next to the `docker-compose.yml` file and `.docker` directory. 

```
docker-compose.yml
.docker/
app/
```

All persisting files generated by the application (such as database data, assets, or gem dependecies) are stored across different volumes which are mounted into the docker containers:

- `app_tmp`: Temporary application data.
- `bundle`: Gem dependencies, managed by *bundler*.
- `node_modules`: Node Modules, managed by *Yarn*.
- `packs`: Compiled assets.
- `uploads`: Uploaded files in `public/uploads`.
- `redis`: Redis data.
- `postgres`: Postgres db data.
